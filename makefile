linux:
	go build

w64:
	GOOS=windows GOARCH=amd64 go build

releases:
	go build
	GOOS=windows GOARCH=amd64 go build
	mv ./8pull.exe ./bin/windows/
	mv ./8pull ./bin/linux/
