# 8Pull v1.1

### Description  
Download any arte video from the url. If only one feed is found, the download will start automatically.

### Releases
You can find a compiled version of this program for linux and windows64 in the `bin` folder.

### Usage  
Usage: 8pull [-v] [--best] [-b value] [-d value] [-h value] [-l value] [-o value] [-q value] [-u value]
     --best               Automatically select the highest quality
     --list=<value>       Display all available option for the specified flag with the specified url
 -b, --bitrate <value>    Filter by bitrate
 -d, --width <value>      Filter by height
 -h, --height <value>     Filter by width
 -l, --lang <value>       Filter by language
 -u, --url <value>        The url leading to the web page to parse
 -q, --quality <value>    Filter by quality
 -o, --out <value>        Path to download the video to
 -v, --verbose            Display additional info

### Examples
List the available feeds with --list to see the availables values for the different flags
```
8pull --url https://www.arte.tv/url/of/the/video --list
```

Or list all the available feeds and chose one in the list
```
8pull --url https://www.arte.tv/url/of/the/video
```

This example will display all the available languages for the given video
```
8pull --url https://www.arte.tv/url/of/the/video --list=lang
```

This example will display all the available qualities for the given video
```
8pull --url https://www.arte.tv/url/of/the/video --list=quality
```


This example will download the video pointed by the url to the path specified and filter the results according to the language (french in this case) and select only the best quality available
```
8pull --url https://www.arte.tv/url/of/the/video --out /path/to/save/the/video/to --best --lang vf
```

This example will display all the available feeds that have an XQ quality (usually the best)
```
8pull --url https://www.arte.tv/url/of/the/video --out /path/to/save/the/video/to --quality XQ
```


This example will evaluate each url in the file "source.txt"
```
8pull --file ./source.txt --out /path/to/save/the/video/to --quality XQ
```

This example will evaluate each url in the file "source.txt"
```
8pull --url https://www.arte.tv/url/of/the/video1,https://www.arte.tv/url/of/the/video2 --out /path/to/save/the/video/to --quality XQ
```
Note that there is no space between each commas


### Notes
Available --quality flag (best to worst) :
+ XQ (1280*720 - AVC1)
+ SQ (1280*720 - MP4)
+ EQ (720*406 - MP4)
+ HQ (640*360 - MP4)
+ MQ (384*216 - MP4)

`XQ` is the best quality  and weights around 1Go per hour of video.

`MQ` is the worst and weights around 150Mo per hour of video.

### Disclaimer
To french users :
**Ripping streamed content is considered as "private copy", which is legal. Mass sharing or unauthorized diffusion is not.**

To others :
**Please check your country's legislation.**
