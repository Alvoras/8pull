package helpers

import (
	"bufio"
	"fmt"
	"gitlab.com/Alvoras/8pull/internal/utils"
	"os"
	"path/filepath"
	"regexp"
)

func IsValidArteURL(url string) (bool, error) {
	return regexp.MatchString(`^((https|http):\/\/)?(www\.)?arte\.tv\/\w{2}\/.*$`, url)
}

func LoadURLsFromFile(path string) ([]string, error) {
	var URLList []string

	abs, err := filepath.Abs(path)
	if err != nil {
		return []string{}, err
	}

	f, err := os.Open(abs)
	if err != nil {
		return []string{}, err
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		URLList = append(URLList, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return []string{}, err
	}

	return URLList, nil
}

func CheckURL(url string) {
	ok, err := IsValidArteURL(url)

	// If URL is not valid
	if !ok || err != nil {
		fmt.Println("Invalid URL")
		os.Exit(1)
	}
}

func AskURL() string {
	var url string
	fmt.Printf("Enter the URL of the video to fetch : ")
	_, err := fmt.Scan(&url)

	utils.Check(err)

	return url
}
