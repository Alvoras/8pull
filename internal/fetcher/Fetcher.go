package fetcher

import (
	"encoding/json"
	"fmt"
	"github.com/briandowns/spinner"
	"github.com/pborman/getopt/v2"
	"gitlab.com/Alvoras/8pull/internal/arte"
	"gitlab.com/Alvoras/8pull/internal/cli"
	"gitlab.com/Alvoras/8pull/internal/utils"
	"net/http"
	"os"
	"os/exec"
	"reflect"
	"strings"
	"time"
)

// type Stream struct {
// 	URL       string
// 	Quality   string
// 	Width     string
// 	Height    string
// 	MediaType string
// 	Bitrate   string
// 	Version   string
// }

type URL struct {
	VideoID string
	Line    string
}

// Fetcher pull info from the web page and download the selected stream
type Fetcher struct {
	Title           string
	Description     string
	LongDescription string
	Out             string
	RawTitle        string
	Selected        arte.Feed
	Feeds           []arte.Feed
	URL             URL
	IsVerbose       bool
}

func NewURL(urlLine string) URL {
	url := URL{
		Line: urlLine,
	}
	url.ParseVideoID()

	return url
}

func (url *URL) ParseVideoID() {
	url.VideoID = strings.Split(url.Line, "/")[5]
}

func (f *Fetcher) RemoveFeed(idx int) {
	f.Feeds = append(f.Feeds[:idx], f.Feeds[idx+1:]...)
}

func (f *Fetcher) AddFeed(feed arte.Feed) {
	f.Feeds = append(f.Feeds, feed)
}

func (f *Fetcher) GetExtension() (string, bool) {
	if strings.Contains(f.Selected.MediaType, "hls") {
		return "mkv", true
	} else if strings.Contains(f.Selected.MediaType, "mp4") {
		return "mp4", true
	}

	return "", false
}

func (f *Fetcher) FetchScriptInfo() *arte.Config {
	cfg := &arte.Config{}
	base := "https://api.arte.tv/api/player/v1/config/fr/"

	res, err := http.Get(base + f.URL.VideoID)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	}

	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&cfg)

	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	}

	f.RawTitle = cfg.VideoJSONPlayer.VTI
	f.Title = strings.Join(strings.Split(strings.Replace(cfg.VideoJSONPlayer.VTI, "/", "-", -1), " "), ".")
	f.Description = cfg.VideoJSONPlayer.V7T
	f.LongDescription = cfg.VideoJSONPlayer.VDE
	// f.Title = cfg.VideoJSONPlayer.VTI

	return cfg
}

func (f *Fetcher) FilterByBitrate(bitrate int) {
	var filtered []arte.Feed
	var feed arte.Feed
	feedsQty := len(f.Feeds)

	for i := 0; i < feedsQty; i++ {
		feed = f.Feeds[i]
		if feed.Bitrate == bitrate {
			filtered = append(filtered, feed)
		}
	}

	f.Feeds = filtered
}

func (f *Fetcher) FilterByWidth(width int) {
	var filtered []arte.Feed
	var feed arte.Feed
	feedsQty := len(f.Feeds)

	for i := 0; i < feedsQty; i++ {
		feed = f.Feeds[i]
		if feed.Width == width {
			filtered = append(filtered, feed)
		}
	}

	f.Feeds = filtered
}

func (f *Fetcher) FilterByHeight(height int) {
	var filtered []arte.Feed
	var feed arte.Feed
	feedsQty := len(f.Feeds)

	for i := 0; i < feedsQty; i++ {
		feed = f.Feeds[i]
		if feed.Height == height {
			filtered = append(filtered, feed)
		}
	}

	f.Feeds = filtered
}

func (f *Fetcher) FilterByQuality(quality string) {
	var filtered []arte.Feed
	var feed arte.Feed
	feedsQty := len(f.Feeds)

	for i := 0; i < feedsQty; i++ {
		feed = f.Feeds[i]
		if feed.Quality == quality {
			filtered = append(filtered, feed)
		}
	}

	f.Feeds = filtered
}

func (f *Fetcher) FilterByLanguage(lang string) {
	// Default
	// Use map[string]struct{} to allow the "if _, ok := aliases[buf.Version]; ok" syntax
	var filtered []arte.Feed
	var feed arte.Feed
	feedsQty := len(f.Feeds)

	var aliases = map[string]struct{}{
		lang: struct{}{},
	}

	switch strings.ToUpper(lang) {
	case "FR", "FRA", "VF", "VOF":
		aliases = map[string]struct{}{
			"VF":  struct{}{},
			"VOF": struct{}{},
		}
	case "GER", "VA", "VOA":
		aliases = map[string]struct{}{
			"VA":  struct{}{},
			"VOA": struct{}{},
		}
	}

	for i := 0; i < feedsQty; i++ {
		feed = f.Feeds[i]
		if _, ok := aliases[feed.Version]; ok {
			filtered = append(filtered, feed)
		}
	}

	f.Feeds = filtered
}

func (f *Fetcher) FilterByBest() {
	// Find highest quality available
	var highest string
	ladder := []string{
		"XQ", // best
		"SQ",
		"EQ",
		"HQ",
		"MQ", // lowest
	}

FindHighest:
	for step := 0; step < len(ladder); step++ {
		for _, feed := range f.Feeds {
			if feed.Quality == ladder[step] {
				highest = ladder[step]
				break FindHighest
			}
		}
	}

	f.FilterByQuality(highest)
}

func (f *Fetcher) Load() {
	cfg := f.FetchScriptInfo()
	rval := reflect.ValueOf(cfg.VideoJSONPlayer.VSR)

	for i := 0; i < rval.NumField(); i++ {
		f.AddFeed(rval.Field(i).Interface().(arte.Feed))
	}
}

func (f *Fetcher) Filter(cli cli.CliArgs, set *getopt.Set) {

	if set.IsSet("best") {
		f.FilterByBest()
	}

	if set.IsSet("quality") {
		f.FilterByQuality(*cli.Quality)
	}

	if set.IsSet("bitrate") {
		f.FilterByBitrate(*cli.Bitrate)
	}

	if set.IsSet("lang") {
		f.FilterByLanguage(*cli.Lang)
	}

	if set.IsSet("width") {
		f.FilterByWidth(*cli.Width)
	}

	if set.IsSet("height") {
		f.FilterByHeight(*cli.Height)
	}
}

func (f *Fetcher) List(param string) {
	if param == "" {
		f.PrintCompatible()
		return
	}

	fmt.Printf("Available values for %s :\n", param)
	for i := 0; i < len(f.Feeds); i++ {
		switch param {
		case "bitrate", "--bitrate", "-b":
			if f.Feeds[i].Bitrate > 0 {
				fmt.Println("[*]", f.Feeds[i].Bitrate)
			}
		case "lang", "--lang", "-l":
			if f.Feeds[i].Version != "" {
				fmt.Printf("[*] %s (%s)\n", f.Feeds[i].Version, f.Feeds[i].VersionLong)
			}
		case "quality", "--quality", "-q":
			if f.Feeds[i].Quality != "" {
				fmt.Println("[*]", f.Feeds[i].Quality)
			}
		case "width", "--width", "-w":
			if f.Feeds[i].Width > 0 {
				fmt.Println("[*]", f.Feeds[i].Width)
			}
		case "height", "--height", "-h":
			if f.Feeds[i].Height > 0 {
				fmt.Println("[*]", f.Feeds[i].Height)
			}
		}
	}
}

func (f *Fetcher) PrintCompatible() {
	for i := 0; i < len(f.Feeds); i++ {
		feed := f.Feeds[i]
		if feed.Quality == "" {
			continue
		}

		fmt.Print(i)
		fmt.Printf("\tQuality : %s\n", feed.Quality)
		fmt.Printf("\tResolution : %d*%d\n", feed.Width, feed.Height)
		fmt.Printf("\tBitrate : %dkbps\n", feed.Bitrate)
		fmt.Printf("\tVersion : %s\n", feed.Version)
		fmt.Printf("\tVersion (Long) : %s\n\n", feed.VersionLong)
	}
}

func (f *Fetcher) SelectFeed() {
	defer func() {
		ext, _ := f.GetExtension()
		f.Title += "." + ext
	}()

	if len(f.Feeds) == 1 {
		f.Selected = f.Feeds[0]
		return
	} else if len(f.Feeds) == 0 {
		fmt.Println("[!] No feed available with these filters")
		os.Exit(0)
	}

	var choice int
	max := len(f.Feeds) - 1

	f.PrintCompatible()

	fmt.Printf("[>] Select a feed (0-%d) :", max)
	_, err := fmt.Scan(&choice)

	utils.Check(err)

	f.Selected = f.Feeds[choice]
}

func (f *Fetcher) Dl() {
	if f.IsVerbose {
		fmt.Printf("[>] %s\n\n", f.RawTitle)
		fmt.Printf("[>] Description :\n")
		fmt.Printf("%s\n\n", f.LongDescription)
	}

	spinnerCharSet := utils.GenerateCharSet(15)

	s := spinner.New(spinnerCharSet, 100*time.Millisecond)
	s.FinalMSG = fmt.Sprintf("[+] \"%s\" has been downloaded to %s as \"%s\"", f.RawTitle, f.Out, f.Title)

	ffmpeg, err := exec.LookPath("ffmpeg")
	if err != nil {
		fmt.Println("[!] Please install ffmpeg to continue")
	}
	utils.Check(err)

	dest := fmt.Sprintf("%s%c%s", f.Out, os.PathSeparator, f.Title)

	line := []string{"-y", "-i", f.Selected.URL, "-c", "copy", "-bsf:a", "aac_adtstoasc", dest}

	cmd := exec.Command(ffmpeg, line...)

	fmt.Println()
	fmt.Printf("[+] Downloading \"%s\"\n", f.RawTitle)
	fmt.Printf("\tQuality : %s\n", f.Selected.Quality)
	fmt.Printf("\tResolution : %d*%d\n", f.Selected.Width, f.Selected.Height)
	fmt.Printf("\tBitrate : %dkbps\n", f.Selected.Bitrate)
	fmt.Printf("\tVersion : %s\n", f.Selected.Version)
	fmt.Printf("\tAs : %s\n\n", dest)

	s.Start()
	err = cmd.Start()
	utils.Check(err)

	err = cmd.Wait()
	utils.Check(err)
	s.Stop()

}
