package cli

// CliArgs is a struc holding all the cli args that the user can control
type CliArgs struct {
	List      *string
	URLList   *string
	Outpath   *string
	Best      *bool
	Verbose   *bool
	Quality   *string
	Bitrate   *int
	Width     *int
	Height    *int
	Lang      *string
	InputFile *string
}

// Cli struct
var Cli CliArgs
