package utils

import (
	"fmt"
	"strings"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func GenerateCharSet(size int) []string {
	var cs []string
	var pad int
	var symbolQty int
	negPad := 0

	for i := 0; i < (size+1)*2; i++ {
		symbolQty = 3
		symbol := strings.Repeat("=", symbolQty)

		if i < size+1 {
			pad = i
		}

		if i == 0 {
			symbol = symbol[1:] + " "
		}

		if i == size {
			symbol = " " + symbol[:symbolQty-1]
		}

		if i > size+1 {
			negPad++
		}

		step := fmt.Sprintf("[%s%s%s]", strings.Repeat(" ", pad-negPad), symbol, strings.Repeat(" ", size-(pad-negPad)))
		cs = append(cs, step)
	}

	return cs
}
