package arte

type Feed struct {
	ID          string `json:"id"`
	Quality     string `json:"quality"`
	Width       int    `json:"width"`
	Height      int    `json:"height"`
	MediaType   string `json:"mediaType"`
	Bitrate     int    `json:"bitrate"`
	URL         string `json:"url"`
	Version     string `json:"versionShortLibelle"`
	VersionLong string `json:"versionLibelle"`
}

type Config struct {
	*VideoJSONPlayer `json:"videoJsonPlayer"`
}

type VideoJSONPlayer struct {
	V7T string `json:"V7T"`
	VDE string `json:"VDE"`
	VTI string `json:"VTI"`
	// Up to 15 differents versions
	VSR struct {
		HTTPS_EQ_15 Feed `json:"HTTPS_EQ_15"`
		HTTPS_MQ_15 Feed `json:"HTTPS_MQ_15"`
		HTTPS_HQ_15 Feed `json:"HTTPS_HQ_15"`
		HTTPS_SQ_15 Feed `json:"HTTPS_SQ_15"`
		HLS_XQ_15   Feed `json:"HLS_XQ_15"`

		HTTPS_EQ_14 Feed `json:"HTTPS_EQ_14"`
		HTTPS_MQ_14 Feed `json:"HTTPS_MQ_14"`
		HTTPS_HQ_14 Feed `json:"HTTPS_HQ_14"`
		HTTPS_SQ_14 Feed `json:"HTTPS_SQ_14"`
		HLS_XQ_14   Feed `json:"HLS_XQ_14"`

		HTTPS_EQ_13 Feed `json:"HTTPS_EQ_13"`
		HTTPS_MQ_13 Feed `json:"HTTPS_MQ_13"`
		HTTPS_HQ_13 Feed `json:"HTTPS_HQ_13"`
		HTTPS_SQ_13 Feed `json:"HTTPS_SQ_13"`
		HLS_XQ_13   Feed `json:"HLS_XQ_13"`

		HTTPS_EQ_12 Feed `json:"HTTPS_EQ_12"`
		HTTPS_MQ_12 Feed `json:"HTTPS_MQ_12"`
		HTTPS_HQ_12 Feed `json:"HTTPS_HQ_12"`
		HTTPS_SQ_12 Feed `json:"HTTPS_SQ_12"`
		HLS_XQ_12   Feed `json:"HLS_XQ_12"`

		HTTPS_EQ_11 Feed `json:"HTTPS_EQ_11"`
		HTTPS_MQ_11 Feed `json:"HTTPS_MQ_11"`
		HTTPS_HQ_11 Feed `json:"HTTPS_HQ_11"`
		HTTPS_SQ_11 Feed `json:"HTTPS_SQ_11"`
		HLS_XQ_11   Feed `json:"HLS_XQ_11"`

		HTTPS_EQ_10 Feed `json:"HTTPS_EQ_10"`
		HTTPS_MQ_10 Feed `json:"HTTPS_MQ_10"`
		HTTPS_HQ_10 Feed `json:"HTTPS_HQ_10"`
		HTTPS_SQ_10 Feed `json:"HTTPS_SQ_10"`
		HLS_XQ_10   Feed `json:"HLS_XQ_10"`

		HTTPS_EQ_9 Feed `json:"HTTPS_EQ_9"`
		HTTPS_MQ_9 Feed `json:"HTTPS_MQ_9"`
		HTTPS_HQ_9 Feed `json:"HTTPS_HQ_9"`
		HTTPS_SQ_9 Feed `json:"HTTPS_SQ_9"`
		HLS_XQ_9   Feed `json:"HLS_XQ_9"`

		HTTPS_EQ_8 Feed `json:"HTTPS_EQ_8"`
		HTTPS_MQ_8 Feed `json:"HTTPS_MQ_8"`
		HTTPS_HQ_8 Feed `json:"HTTPS_HQ_8"`
		HTTPS_SQ_8 Feed `json:"HTTPS_SQ_8"`
		HLS_XQ_8   Feed `json:"HLS_XQ_8"`

		HTTPS_EQ_7 Feed `json:"HTTPS_EQ_7"`
		HTTPS_MQ_7 Feed `json:"HTTPS_MQ_7"`
		HTTPS_HQ_7 Feed `json:"HTTPS_HQ_7"`
		HTTPS_SQ_7 Feed `json:"HTTPS_SQ_7"`
		HLS_XQ_7   Feed `json:"HLS_XQ_7"`

		HTTPS_EQ_6 Feed `json:"HTTPS_EQ_6"`
		HTTPS_MQ_6 Feed `json:"HTTPS_MQ_6"`
		HTTPS_HQ_6 Feed `json:"HTTPS_HQ_6"`
		HTTPS_SQ_6 Feed `json:"HTTPS_SQ_6"`
		HLS_XQ_6   Feed `json:"HLS_XQ_6"`

		HTTPS_EQ_5 Feed `json:"HTTPS_EQ_5"`
		HTTPS_MQ_5 Feed `json:"HTTPS_MQ_5"`
		HTTPS_HQ_5 Feed `json:"HTTPS_HQ_5"`
		HTTPS_SQ_5 Feed `json:"HTTPS_SQ_5"`
		HLS_XQ_5   Feed `json:"HLS_XQ_5"`

		HTTPS_EQ_4 Feed `json:"HTTPS_EQ_4"`
		HTTPS_MQ_4 Feed `json:"HTTPS_MQ_4"`
		HTTPS_HQ_4 Feed `json:"HTTPS_HQ_4"`
		HTTPS_SQ_4 Feed `json:"HTTPS_SQ_4"`
		HLS_XQ_4   Feed `json:"HLS_XQ_4"`

		HTTPS_EQ_3 Feed `json:"HTTPS_EQ_3"`
		HTTPS_MQ_3 Feed `json:"HTTPS_MQ_3"`
		HTTPS_HQ_3 Feed `json:"HTTPS_HQ_3"`
		HTTPS_SQ_3 Feed `json:"HTTPS_SQ_3"`
		HLS_XQ_3   Feed `json:"HLS_XQ_3"`

		HTTPS_EQ_2 Feed `json:"HTTPS_EQ_2"`
		HTTPS_MQ_2 Feed `json:"HTTPS_MQ_2"`
		HTTPS_HQ_2 Feed `json:"HTTPS_HQ_2"`
		HTTPS_SQ_2 Feed `json:"HTTPS_SQ_2"`
		HLS_XQ_2   Feed `json:"HLS_XQ_2"`

		HTTPS_EQ_1 Feed `json:"HTTPS_EQ_1"`
		HTTPS_MQ_1 Feed `json:"HTTPS_MQ_1"`
		HTTPS_HQ_1 Feed `json:"HTTPS_HQ_1"`
		HTTPS_SQ_1 Feed `json:"HTTPS_SQ_1"`
		HLS_XQ_1   Feed `json:"HLS_XQ_1"`
	} `json:"VSR"`
}
