package main

import (
	"fmt"
	"github.com/pborman/getopt/v2"
	"gitlab.com/Alvoras/8pull/internal/cli"
	"gitlab.com/Alvoras/8pull/internal/fetcher"
	"gitlab.com/Alvoras/8pull/internal/helpers"
	"os"
	"strings"
)

func init() {
	cli.Cli.List = getopt.StringLong("list", 0, "", "Display all available option for the specified flag with the specified url")
	getopt.Lookup("list").SetOptional()
	cli.Cli.URLList = getopt.StringLong("url", 'u', "", "The URLs leading to the web page to parse")
	cli.Cli.Quality = getopt.StringLong("quality", 'q', "", "Filter by quality")
	cli.Cli.Best = getopt.BoolLong("best", 0, "Automatically select the highest quality")
	cli.Cli.Bitrate = getopt.IntLong("bitrate", 'b', 0, "Filter by bitrate")
	cli.Cli.Width = getopt.IntLong("width", 'h', 0, "Filter by width")
	cli.Cli.Height = getopt.IntLong("height", 'd', 0, "Filter by height")
	cli.Cli.Lang = getopt.StringLong("lang", 'l', "", "Filter by language")
	cli.Cli.Outpath = getopt.StringLong("out", 'o', ".", "Path of the downloaded video")
	cli.Cli.Verbose = getopt.BoolLong("verbose", 'v', "Display additional info")
	cli.Cli.InputFile = getopt.StringLong("file", 'f', "Specify an input source file with one url per line")

	getopt.Parse()

	// if getopt.CommandLine.IsSet("url") {
	// 	helpers.CheckURL(*cli.Cli.URL)
	// }
}

func main() {
	var URLList []string
	var err error

	if !getopt.CommandLine.IsSet("url") && !getopt.CommandLine.IsSet("file") {
		url := helpers.AskURL()
		URLList = []string{url}
	} else if getopt.CommandLine.IsSet("url") {
		URLList = strings.Split(*cli.Cli.URLList, ",")
	} else if getopt.CommandLine.IsSet("file") {
		if URLList, err = helpers.LoadURLsFromFile(*cli.Cli.InputFile); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	for _, url := range URLList {
		if ok, err := helpers.IsValidArteURL(url); !ok || err != nil {
			fmt.Println(err)
			continue
		}

		engine := fetcher.Fetcher{
			URL:       fetcher.NewURL(url),
			Out:       *cli.Cli.Outpath,
			IsVerbose: *cli.Cli.Verbose,
		}

		engine.Load()

		if getopt.CommandLine.IsSet("list") {
			engine.List(*cli.Cli.List)
			continue
		}

		engine.Filter(cli.Cli, getopt.CommandLine)

		engine.SelectFeed()

		engine.Dl()
	}
}
